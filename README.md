# SHAPESHIFTER OPEN SOURCE DRUM MACHINE

Shapeshifter is an Open Source and Open Hardware drum machine with the powerful Teensy 3.6 development board at his core.

The basic structure is composed of 22 buttons, 22 LEDs, 3 potentiometers, a small 170 points breadboard, and a slot for custom expansion boards.

You can experiment with the breadboard, spare components and code to make new functionalities or improve the existing one. When you are satisfied, you can make your own expansion board.
## Assembly

Shapeshifter has been designed with THT components to be easy to assemble, anyone can build his Drum Machine in a few hours of work and with little or no previous experience.

## Hardware and Software
Shapeshifter is powered by the [Teensy 3.6 board](https://choosealicense.com/licenses/mit/) with a powerful 32 bit 180 MHz ARM Cortex-M4 Core Processor. 

*The Teensy is a complete USB-based microcontroller development system, in a very small footprint, capable of implementing many types of projects. All programming is done via the USB port using the renowned Arduino programming language.*

## Community driven ##
Shapeshifter wants to be a community-driven project, his opensource nature gives you the opportunity to modify his hardware and his software functionalities according to your needs. Please visit our forum at [Faselunare.com](http://www.faselunare.com) and subscribe to our newsletter to stay informed. 

## License ##
Code: GPL3.0.

Schematics: cc-by-sa-3.0

By: Faselunare (info@faselunare.com)

## Guidelines for derivative works ##
Faselunare is a registered trademark.

The name "Faselunare" and his logotype should not be used on any of the derivative works you create from these files.

We do not recommend you to keep the "Shapeshifter" original name for your derivative works.

![image](https://gitlab.com/Faselunare/shapeshifter/raw/master/Images/shapeshifter-render-front.jpg)
