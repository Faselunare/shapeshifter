/*
 * SHAPESHIFTER DIY OPEN SOURCE DRUM MACHINE
 * by FASELUNARE.COM
 * based on the DIY DRUM MACHINE by Sebastian Tomczak
 *  
 * https://github.com/faselunare/shapeshifter 
 * Released under the GPLv3 license. 
 * 
 */

#ifndef PINASSIGNMENT_H
#define PINASSIGNMENT_H

#include "Constants.h"

#define SDCARD_CS_PIN       BUILTIN_SDCARD
#define SDCARD_MOSI_PIN     11  // not actually used
#define SDCARD_SCK_PIN      13  // not actually used

#define TRACKA_BUTTON       30
#define TRACKB_BUTTON       28
#define TRACKC_BUTTON       26
#define TRACKD_BUTTON       24
#define FN_BUTTON           9
#define PLAY_BUTTON         10
#define SAVE_BUTTON         11
#define LOAD_BUTTON         13

#define TRACKA_LED          29
#define TRACKB_LED          27
#define TRACKC_LED          25
#define TRACKD_LED          12
#define FN_LED              5
#define PLAY_LED            6

// 74HC595 Shift Register Configuration and Variables
const uint8_t clockPin = 0;          // Teensy digital pin connected to SH_CP Pin 11 of 74HC595
const uint8_t latchPin = 1;    // Teensy digital pin connected to ST_CP Pin 12 of 74HC595
const uint8_t dataPin = 2  ;           // Teensy digital pin connected to DS of Pin 14 of 74HC595

// Button, Pots and LEDs Configuration
const uint8_t patternButtons[NUMBER_OF_STEPS] = {33, 34, 35, 36, 37, 38, 39, 15, 16, 17, 18, 19, 20, 21, 22, 23};

const uint8_t lowPassPot = A12;
const uint8_t tempoPot = A13;
const uint8_t bitCrushPot = A0;

const uint8_t tracksButtons[NUMBER_OF_TRACKS] = {TRACKA_BUTTON, TRACKB_BUTTON, TRACKC_BUTTON, TRACKD_BUTTON};
const uint8_t tracksLEDs[NUMBER_OF_TRACKS] = {TRACKA_LED, TRACKB_LED, TRACKC_LED, TRACKD_LED};

const uint8_t controlButtons[NUMBER_OF_CONTROLS] = {TRACKA_BUTTON, TRACKB_BUTTON, TRACKC_BUTTON, TRACKD_BUTTON, FN_BUTTON, PLAY_BUTTON};
const uint8_t controlLEDs[NUMBER_OF_CONTROLS] = {TRACKA_LED, TRACKB_LED, TRACKC_LED, TRACKD_LED, FN_LED, PLAY_LED};

#endif